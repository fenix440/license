## License (Package) ##

Contains various license files for different projects / packages. __Unless you are working with/on Fenix440 projects, this package is very useless for you!__

Official website (https://bitbucket.org/fenix440/license)

## Contents ##

[TOC]

## When to use this ##

If you are working with me one or several of my projects, then you can use this “license repository” in order to fetch the appropriate license file.

## How to install ##

```
#!console

composer require fenix440/license
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.


## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package